const approuter = require("@sap/approuter");
const jwtDecode = require('jwt-decode');

const ar = approuter();

// add custom middleware
ar.beforeRequestHandler
    .use('/me', function udinaMiddleware(req, res) {
        if (!req.user) {
            res.statusCode = 403;
            res.end('Missing JWT Token');
        } else {
            const { user } = req
            const token = jwtDecode(user.token.accessToken);

            res.statusCode = 200;
            res.setHeader("Content-type", "application/json");
            res.end(JSON.stringify({
                "loginName": user.id,
                "firstName": token.given_name,
                "lastName": token.family_name,
                "email": token.email,
                // missing additional properties so far!
                "origin": token.origin,
                "roles": token["xs.system.attributes"]["xs.rolecollections"],
                "sessionTimeout": req.routerConfig.sessionTimeout
            }));            
        }
    });

// start the appouter
ar.start();